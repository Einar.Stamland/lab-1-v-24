package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    boolean continuePlaying = true;
    Random random = new Random();
    
    public void run() {
        sc = new Scanner(System.in);
        do {
            int randomIndex = random.nextInt(rpsChoices.size());
            String computerMove = rpsChoices.get(randomIndex);

            System.out.println("Let's play round " + roundCounter +"\n"+"Your choice (Rock/Paper/Scissors)?");
            String playerMove = sc.nextLine();
            if (!rpsChoices.contains(playerMove)) {
                System.out.println("I do not understand " + playerMove +". Could you try again?\n" + "Your choice (Rock/Paper/Scissors)?");
                playerMove = sc.nextLine();
            }
            
            if (playerMove.equals(computerMove)) {
                System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" it's a tie!\nScore: human " + humanScore+ " computer " +computerScore );
            }

            else if (playerMove.equals("rock")) {
                if (computerMove == "Paper") {
                    computerScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Computer wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
                else{
                    humanScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Human wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
            
            }
            else if (playerMove.equals("paper")) {
                if (computerMove == "Scissors") {
                    computerScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Computer wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
                else{
                    humanScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Human wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
            }
            else if (playerMove.equals("scissors")) {
                if (computerMove == "Rock") {
                    computerScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Computer wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
                else{
                    humanScore++;
                    System.out.println("Human chose " + playerMove+ ", computer chose " + computerMove +" Human wins!\nScore: human " + humanScore+ " computer " +computerScore );
                }
            }
            System.out.println("Do you wish to continue playing? (y/n)?");
            String newRound = sc.nextLine();
            if (newRound.equals("y")) {
                roundCounter ++;
            }
            else if (newRound.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
        while (continuePlaying = true);
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}