package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int longestWord = word1.length();
        if (word2.length() > longestWord ) {
            longestWord = word2.length();
        }
        if (word3.length()> longestWord) {
            longestWord = word3.length();
        }
        if (word1.length() == longestWord){
            System.out.println(word1);
        }if (word2.length() == longestWord){
            System.out.println(word2);
        }if (word3.length() == longestWord){
            System.out.println(word3);
        }
            
        
    }

    public static boolean isLeapYear(int year) {
        if (year %4 !=0) {
            return false;
        }
        else if (year %100 ==0) {
            if (year % 400 ==0) {
                return true;
            }
            else return false;
            
        }
        
        return true;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num %2 == 0) {
            return true;
        }
        else return false;
    }

}
