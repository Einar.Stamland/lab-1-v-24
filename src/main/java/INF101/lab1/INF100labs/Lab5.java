package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multipliedValues = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            int valueA = list.get(i);
            int valueB = 2 * valueA;
            multipliedValues.add(valueB);
        }
        return multipliedValues;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == 3) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> noDuplicates = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (noDuplicates.contains(list.get(i))) {
                
            }
            else noDuplicates.add(list.get(i));
        }
        return noDuplicates;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            int valuea_2 = a.get(i) + b.get(i);
            a.set(i, valuea_2);
        }
    }

}