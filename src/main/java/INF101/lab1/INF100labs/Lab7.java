package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> sumRow = new ArrayList<>();
        ArrayList<Integer> sumCols = new ArrayList<>();
        for (int i = 0; i < grid.size(); i++) {
            int valueRow = 0;
            int valueCols = 0;
            for (int j = 0; j < grid.get(i).size(); j++) {
                valueRow = valueRow + grid.get(i).get(j);
                valueCols = valueCols + grid.get(j).get(i);
            }
            sumRow.add(valueRow);
            sumCols.add(valueCols);
        }

        boolean allEqualRows = sumRow.stream().distinct().limit(2).count() <= 1;
        boolean allEqualCols = sumCols.stream().distinct().limit(2).count() <= 1;
        if (allEqualRows && allEqualCols == true){
            return true;
        }
        else return false;

    }

}