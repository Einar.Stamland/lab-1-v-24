package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 1; i < n +1; i++) {
            if (i %7 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i < n+1; i++){
            ArrayList<Integer> allNumbers = new ArrayList<Integer>();
            for (int j = 1; j < n+1; j++) {
                allNumbers.add(i*j);
            }
            String listString = allNumbers.stream().map(Object::toString)
            .collect(Collectors.joining(" "));
            System.out.println(i+": "+ listString);
        }

    }

    public static int crossSum(int num) {
        int i = 0;
        while (num % 10 > 0 ){
            i = i + num%10;
            num = num /10;
        }
        return i;

    }

}